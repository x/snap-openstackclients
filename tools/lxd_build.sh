#!/bin/bash

set -ex

sudo apt update

export PATH=/snap/bin:$PATH

# Setup snapd and snapcraft
sudo apt install -y snapd nftables
sudo snap install --classic snapcraft
sudo snap install --classic lxd
sudo lxd init --auto

sudo nft insert rule filter openstack-INPUT iif lxdbr0 accept

# Build our snap!
sudo snapcraft --use-lxd
