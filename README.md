# OpenStack Clients Snap

This repository contains the source code of the snap for all OpenStack Client
tools.

## Installing this snap

The openstackclients snap can be installed directly from the snap store:

    sudo snap install openstackclients

## Support

Please report any bugs related to this snap on
[Launchpad](https://bugs.launchpad.net/snap-openstackclients/+filebug).

Alternatively you can find the OpenStack Snap team in `#openstack-snaps`
on Freenode IRC.
